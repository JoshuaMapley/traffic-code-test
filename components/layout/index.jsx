import Styled from "@emotion/styled";
import css from "@styled-system/css";

const Main = Styled("main")({
  position: "relative",
});

const Container = Styled("div")(
  css({
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
    alignItems: ["flex-start", "center"],
    width: "100%",
    maxWidth: 1294,
    pt: [62, 0],
    px: 20,
    mx: "auto",
    flex: 1,
  })
);

const PageWrapper = Styled("div")(
  css({
    display: "flex",
    flexDirection: "column",
  })
);

const Hero = Styled("div")(
  css({
    display: "flex",
    width: "100%",
    height: ["calc(100vh - 50px)", "calc(100vh - 90px)"],
  })
);

export { Main, Container, PageWrapper, Hero };
