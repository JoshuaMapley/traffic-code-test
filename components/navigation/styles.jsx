import Styled from "@emotion/styled";
import css from "@styled-system/css";

const Wrapper = Styled("div")(
  css({
    display: "flex",
    flexDirection: "column",
    position: "relative",
    height: [50, 90],
    width: "100%",
    backgroundColor: "#fff",
    boxShadow: "0px 2px 2px rgba(0, 0, 0, 0.05)",
  })
);

const AccentBar = Styled("div")(
  css({
    height: [10, 32],
    width: "100%",
    backgroundColor: "rgba(186, 217, 130, 0.7)",
  })
);

const Container = Styled("div")(
  css({
    position: "relative",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    width: "100%",
    maxWidth: 1294,
    px: 20,
    mx: "auto",
    flex: 1,
  })
);

const LogoBox = Styled("div")(
  css({
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: [121, 244],
    height: [75, 138],
    left: 20,
    top: [-10, -32],
    backgroundColor: "#333",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    zIndex: 10,
    img: {
      width: [78.33, 145],
    },
  })
);

const NavItems = Styled("div")(
  css({
    display: "grid",
    alignItems: "center",
    gridTemplateColumns: "auto auto",
    gridColumnGap: [25, 45],
  })
);

const Item = Styled("a")(
  css({
    "position": "relative",
    "width": "fit-content",
    "display": "flex",
    "fontSize": "14px",
    "lineHeight": "20px",
    "fontWeight": "700",
    "&:after": {
      content: "''",
      width: "100%",
      height: "2px",
      backgroundColor: "#333",
      position: "absolute",
      left: 0,
      opacity: 0,
      transform: "translateY(5px)",
      bottom: "-5px",
      zIndex: 1,
      transition: "ease all 0.2s",
    },
    "span": {
      display: ["none", "block"],
    },
    "img": {
      mr: "7px",
    },
    "&:hover": {
      "&:after": {
        content: "''",
        width: "100%",
        height: "2px",
        backgroundColor: "#333",
        position: "absolute",
        left: 0,
        bottom: "-5px",
        zIndex: 1,
        opacity: 1,
        transform: "translateY(0px)",
      },
    },
  })
);

export { Wrapper, AccentBar, Container, LogoBox, NavItems, Item };
