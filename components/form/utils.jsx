import * as Yup from "yup";
import "yup-phone";

export const schemaOptions = {
  firstName: Yup.string()
    .min(2, "This name is too short.")
    .max(50, "This name is too long.")
    .required("A name is required."),
  email: Yup.string()
    .email("This email address is invalid.")
    .required("We require a valid email address."),
  phone: Yup.string()
    .phone("AU", true, "Please enter a valid Australian phone number.")
    .required("We require a valid Australian phone number"),
  timeFrame: Yup.string().required("Please select an option from the list."),
  services: Yup.array()
    .min(2, "Please select a minimum of 2 services.")
    .max(5, "Please select fewer than 5 services.")
    .required("A minimum of 2 services are required."),
};
