import React, { useState } from "react";
import axios from "axios";
import { Formik, ErrorMessage } from "formik";
import {
  H3,
  Button,
  ButtonWrapper,
  Form,
  Field,
  ErrorWrapper,
  CheckboxWrapper,
  CheckItem,
  CheckLabel,
  CustomCheck,
  CheckText,
  SubmittedWrapper,
} from "./styles";
import { Persist } from "formik-persist";
import { schemaOptions } from "./utils";
import * as Yup from "yup";
import "yup-phone";

const formSchema = Yup.object().shape(schemaOptions);

const GeneralForm = () => {
  /* Server State Handling */
  const [serverState, setServerState] = useState();
  const handleServerResponse = (ok, msg) => {
    setServerState({ ok, msg });
  };
  const handleOnSubmit = (values, actions) => {
    axios({
      method: "POST",
      url: "https://formspree.io/f/xleoqkeb",
      data: values,
    })
      .then((response) => {
        actions.setSubmitting(false);
        actions.resetForm();
        handleServerResponse(
          true,
          "Thank you for your submission! We'll get back to you soon."
        );
      })
      .catch((error) => {
        actions.setSubmitting(false);
        handleServerResponse(false, error.response.data.error);
      });
  };
  return (
    <>
      <H3>
        Be the first to register for new townhome releases for first option
      </H3>
      <Formik
        initialValues={{
          firstName: "",
          email: "",
          phone: "",
          timeFrame: "",
          services: [],
        }}
        onSubmit={handleOnSubmit}
        validationSchema={formSchema}
      >
        {({ isSubmitting }) => (
          <Form
            id="fs-frm"
            noValidate
            onChange={(values) => console.log(values)}
          >
            <Field
              id="firstName"
              type="text"
              name="firstName"
              placeholder="First Name *"
            />
            <ErrorMessage name="firstName">
              {(msg) => <ErrorWrapper>{msg}</ErrorWrapper>}
            </ErrorMessage>
            <Field id="email" type="email" name="email" placeholder="Email *" />
            <ErrorMessage name="email">
              {(msg) => <ErrorWrapper>{msg}</ErrorWrapper>}
            </ErrorMessage>
            <Field id="phone" type="tel" name="phone" placeholder="Phone *" />
            <ErrorMessage name="phone">
              {(msg) => <ErrorWrapper>{msg}</ErrorWrapper>}
            </ErrorMessage>
            <Field as="select" name="timeFrame">
              <option value="">When are you looking to buy? *</option>
              <option value="1 - 6 months">1 - 6 months</option>
              <option value="6 - 12 months">6 - 12 months</option>
              <option value="1 - 2 years">1 - 2 years</option>
            </Field>
            <ErrorMessage name="timeFrame">
              {(msg) => <ErrorWrapper>{msg}</ErrorWrapper>}
            </ErrorMessage>
            <CheckboxWrapper role="group" aria-labelledby="checkbox-group">
              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="One" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service one</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Two" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service two</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Three" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service three</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Four" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service four</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Five" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service five</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Six" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service six</CheckText>
              </CheckItem>

              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Seven" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service seven</CheckText>
              </CheckItem>
              <CheckItem>
                <CheckLabel>
                  <Field type="checkbox" name="services" value="Eight" />
                  <CustomCheck />
                </CheckLabel>
                <CheckText>Service eight</CheckText>
              </CheckItem>
            </CheckboxWrapper>
            <ErrorMessage name="services">
              {(msg) => <ErrorWrapper>{msg}</ErrorWrapper>}
            </ErrorMessage>

            <ButtonWrapper>
              <Button type="submit" disabled={isSubmitting}>
                Submit
              </Button>
              <Button
                secondary
                type="reset"
                onClick={(formProps) => formProps.resetForm}
              >
                Reset
              </Button>
            </ButtonWrapper>
            {serverState && (
              <SubmittedWrapper className={!serverState.ok ? "errorMsg" : ""}>
                {serverState.msg}
              </SubmittedWrapper>
            )}
            <Persist name="signup-form" />
          </Form>
        )}
      </Formik>
    </>
  );
};

export default GeneralForm;
