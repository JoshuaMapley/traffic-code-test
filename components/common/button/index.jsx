import Styled from "@emotion/styled";
import css from "@styled-system/css";

const Button = Styled("a")(
  css({
    display: ["block", "none"],
    width: "fit-content",
    py: "10px",
    px: 16,
    backgroundColor: "#BAD982",
    fontFamily: "Josefin Sans, sans-serif",
    fontSize: 16,
    color: "#333",
    lineHeight: "26px",
    textTransform: "uppercase",
    borderRadius: 4,
    border: "none",
  })
);

export { Button };
