import { SwiperSlide } from "swiper/react";
import SwiperCore, {
  EffectFade,
  Autoplay,
  Navigation as SwipeNav,
} from "swiper";
import { Swiper, Slide } from "./styles";

SwiperCore.use([EffectFade, Autoplay, SwipeNav]);

const Slider = () => {
  return (
    <Swiper
      spaceBetween={50}
      slidesPerView={1}
      effect="fade"
      loop="true"
      autoplay={{ delay: 10000, disableOnInteraction: false }}
    >
      <SwiperSlide>
        <Slide src="/images/slides/slide_1.jpg" />
      </SwiperSlide>
      <SwiperSlide>
        <Slide src="/images/slides/slide_2.jpg" />
      </SwiperSlide>
      <SwiperSlide>
        <Slide src="/images/slides/slide_3.jpg" />
      </SwiperSlide>
    </Swiper>
  );
};

export default Slider;
