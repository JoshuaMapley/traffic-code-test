import Styled from "@emotion/styled";
import css from "@styled-system/css";
import { Swiper as SwiperBase } from "swiper/react";

const Slide = Styled("img")(
  css({
    width: "100%",
    height: ["calc(100vh - 50px)", "calc(100vh - 90px)"],
    objectFit: "cover",
    objectPosition: "center center",
  })
);

const Swiper = Styled(SwiperBase)(
  css({
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: ["calc(100vh - 50px)", "calc(100vh - 90px)"],
    zIndex: -2,
  })
);
export { Slide, Swiper };
