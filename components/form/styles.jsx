import Styled from "@emotion/styled";
import css from "@styled-system/css";
import { Form as FormBase, Field as FieldBase, ErrorMessage } from "formik";

const H3 = Styled("h1")(
  css({
    fontFamily: "Roboto Slab, sans-serif",
    fontWeight: 700,
    fontSize: 24,
    lineHeight: "28px",
    letterSpacing: 0.176471,
    color: "#333",
    mt: 0,
    mb: 24,
  })
);

const Button = Styled("button")(({ secondary }) =>
  css({
    "py": "10px",
    "backgroundColor": secondary ? "#35ABD1" : "#BAD982",
    "fontFamily": "Josefin Sans, sans-serif",
    "fontSize": 16,
    "color": secondary ? "#fff" : "#333",
    "lineHeight": "26px",
    "textTransform": "uppercase",
    "borderRadius": 4,
    "border": "none",
    "transition": "ease all 0.2s",
    "&:hover": {
      transform: "translateY(-2px)",
      boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
      cursor: "pointer",
    },
    "&:focus": {
      outline: "none",
    },
  })
);

const ButtonWrapper = Styled("div")(
  css({
    display: "grid",
    gridTemplateColumns: "repeat(2, 1fr)",
    gridColumnGap: "8px",
    pt: 24,
  })
);

const Form = Styled(FormBase)(
  css({
    display: "flex",
    flexDirection: "column",
  })
);

const Field = Styled(FieldBase)(
  css({
    "padding": 16,
    "mb": "8px",
    "background":
      "linear-gradient(0deg, rgba(187, 189, 183, 0.1), rgba(187, 189, 183, 0.1)), #FFFFFF",
    "border": "0.5px solid #0b284682",
    "boxSizing": "border-box",
    "borderRadius": "4px",
    "fontWeight": 400,
    "fontFamily": "Roboto, sans-serif",
    "fontSize": 16,
    "lineHeight": "26px",
    "color": "#292F2D",
    "letterSpacing": "0.07px",
    "&:focus": {
      outlineColor: "#BAD982",
    },
  })
);

const SubmittedWrapper = Styled("div")(
  css({
    color: "#333",
    fontSize: 16,
    backgroundColor: "#cee4a7",
    border: "1px solid #BAD982",
    textAlign: "center",
    px: "32px",
    py: "16px",
    mt: "40px",
    borderRadius: "4px",
  })
);

const ErrorWrapper = Styled("div")(
  css({
    color: "#fff",
    fontSize: 12,
    backgroundColor: "#d98282",
    border: "1px solid #cc4444",
    px: "16px",
    py: "8px",
    mb: "8px",
    borderRadius: "4px",
  })
);

const CheckboxWrapper = Styled("div")(
  css({
    mt: "5px",
    display: "flex",
    flexDirection: "column",
  })
);

const CheckItem = Styled("div")(
  css({
    display: "flex",
    justifyContent: "flex-start",
    mb: "8px",
  })
);

const DesktopWrapper = Styled("div")(
  css({
    display: ["none", "flex"],
    flexDirection: "column",
    width: "100%",
    maxWidth: 520,
    backgroundColor: "#FFFFFF",
    boxSizing: "border-box",
    borderRadius: "4px",
    padding: 40,
    boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
  })
);

const MobileWrapper = Styled("div")(
  css({
    position: "relative",
    display: ["flex", "none"],
    flexDirection: "column",
    flex: 1,
    padding: 16,
    backgroundColor: "#E5E5E5",
  })
);

const CheckLabel = Styled("label")`
display: block;
position: relative;
cursor: pointer;
font-size: 22px;
line-height: 24px;
height: 24px;
width: 24px;
clear: both;
& > input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
};
& > input:checked ~ span {
    background-color: #BAD982;
    border-radius: 5px;
    -webkit-transform: rotate(0deg) scale(1);
    -ms-transform: rotate(0deg) scale(1);
    transform: rotate(0deg) scale(1);
    opacity:1;
    border: 0.5px solid #333;
};
& > input:checked ~ span::after {
    -webkit-transform: rotate(45deg) scale(1);
    -ms-transform: rotate(45deg) scale(1);
    transform: rotate(45deg) scale(1);
    opacity:1;
    left: 7px;
    top: 2px;
    width: 6px;
    height: 12px;
    border: solid #333;
    border-width: 0 2px 2px 0;
    background-color: transparent;
    border-radius: 0;
  };
`;

const CustomCheck = Styled("span")(
  css({
    "position": "absolute",
    "top": "0px",
    "left": "0px",
    "height": "24px",
    "width": "24px",
    "backgroundColor": "transparent",
    "borderRadius": "5px",
    "transition": "all 0.3s ease-out",
    "border": "0.5px solid #333",
    "&::after": {
      position: "absolute",
      content: '""',
      left: "12px",
      top: "12px",
      height: "0px",
      width: "0px",
      borderRadius: "5px",
      border: "solid #009BFF",
      borderWidth: "0 3px 3px 0",
      transform: "rotate(0deg) scale(0)",
      opacity: 1,
      transition: "all 0.3s ease-out",
    },
  })
);

const CheckText = Styled("p")(
  css({
    fontFamily: "Roboto, sans-serif",
    mb: 0,
    mt: "3px",
    ml: "8px",
    fontSize: 13,
    lineHeight: "20px",
  })
);
export {
  H3,
  Button,
  ButtonWrapper,
  Form,
  Field,
  ErrorWrapper,
  CheckboxWrapper,
  CheckItem,
  CheckLabel,
  DesktopWrapper,
  MobileWrapper,
  CustomCheck,
  CheckText,
  SubmittedWrapper,
};
